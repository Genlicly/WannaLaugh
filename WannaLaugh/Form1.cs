﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace WannaLaugh
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeComponent();
            timer1.Tick += timer1_Tick;
            timer1.Start();
            SoundPlayer audio = new SoundPlayer(WannaLaugh.Properties.Resources.output);
            audio.Play();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Label11.Text = DateTime.Now.ToString("hh:mm:ss");
            Label12.Text = DateTime.Now.ToString("hh:mm:ss");
            Label14.Text = DateTime.Now.ToString("hh:mm:ss");
            Label15.Text = DateTime.Now.ToString("hh:mm:ss");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("咁快走 :(", "同我玩多陣吖 ._.", MessageBoxButtons.OK, MessageBoxIcon.Information);

            int numberOfTimesToShowMessage_1 = 3;
            int numberOfTimesToShowMessage_2 = 10;
            int numberOfTimesToShowMessage_3 = 999;

            for (int i = 0; i < numberOfTimesToShowMessage_1; i++)
            {
                MessageBox.Show("唔俾你走 !!", "真係咁想走 ?", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            for (int i = 0; i < numberOfTimesToShowMessage_2; i++)
            {
                MessageBox.Show("haha 你走唔到!!!", "你走唔到 :)", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            for (int i = 0; i < numberOfTimesToShowMessage_3; i++)
            {
                MessageBox.Show("傻仔，走唔到 ~", "!!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
